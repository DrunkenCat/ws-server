package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

type Router struct {
	http *mux.Router
	ws   *mux.Router
	cnt  *Container
}

var router = &Router{}

func GetRouter(c *Container) *Router {
	if router != nil {
		http := mux.NewRouter()
		ws := mux.NewRouter()

		http.HandleFunc("/pub/{channel}/", router.PubHandler)
		http.HandleFunc("/sub/{channel}/{id}/", router.SubHandler)
		http.HandleFunc("/open/{channel}/", router.OpenHandler)
		http.HandleFunc("/close/{channel}/", router.CloseHandler)
		http.HandleFunc("/unsub/{channel}/{id}/", router.UnsubHandler)
		http.HandleFunc("/stats/", router.StatsHandler)
		ws.HandleFunc("/ws/", router.SocketHandler)

		c.router = &Router{
			http: http,
			ws:   ws,
		}

		router.cnt = c
	}
	return router
}

func (r *Router) PubHandler(w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	ch := container.GetChannel(vars["channel"])
	log.Println("PubHandler")
	if ch != nil {
		ch.Publish([]byte(vars["data"]))
	}
}

func (r *Router) SubHandler(w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	c := container.GetClient(vars["id"])
	log.Println("SubHandler")
	if c != nil {
		c.Subscribe(vars["channel"])
	}
}

func (r *Router) UnsubHandler(w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	c := container.GetClient(vars["id"])
	log.Println("UnsubHandler")
	if c != nil {
		c.Unsubscribe(vars["channel"])
	}
}

func (r *Router) OpenHandler(w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	container.AddChannel(vars["channel"])
	log.Println("OpenHandler")
}

func (r *Router) CloseHandler(w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	container.RemoveChannel(vars["channel"])
	log.Println("CloseHandler")
}

func (r *Router) SocketHandler(w http.ResponseWriter, req *http.Request) {
	log.Println("SocketHandler")
	ws, err := upgrader.Upgrade(w, req, nil)
	if err != nil {
		log.Println(err)
		return
	}
	vars := mux.Vars(req)
	client := &Client{ws: ws, send: make(chan []byte, 256)}
	container.AddClient(vars["id"], client)

	go client.WritePump()
	go client.ReadPump()
}

func (r *Router) StatsHandler(w http.ResponseWriter, req *http.Request) {
	// log.Println(r.cnt.stats)
	// json, err := json.Marshal(r.cnt.stats)
	// if err == nil {
	// 	w.Write(json)
	// }
	log.Println("Stats")
}
