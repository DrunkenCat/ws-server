package main

import (
	"time"
)

const subDuration = time.Minute * 30

type Channel struct {
	clients    map[*Client]time.Time
	broadcast  chan []byte
	register   chan *Client
	unregister chan *Client
	Name       string
	active     bool
	stats      *ServerStats
}

func (ch *Channel) Publish(data []byte) {
	ch.broadcast <- data
}

func (ch *Channel) run() {
	for {
		select {
		case client := <-ch.register:
			ch.clients[client] = time.Now()
		case client := <-ch.unregister:
			if _, ok := ch.clients[client]; ok {
				delete(ch.clients, client)
				close(client.send)
			}
		case message := <-ch.broadcast:
			for client := range ch.clients {
				select {
				case client.send <- message:
				default:
					close(client.send)
					delete(ch.clients, client)
				}
			}

		}
		if ch.active == false {
			break
		}
	}
}

func (ch *Channel) ClearSubscriptions() {
	for client, date := range ch.clients {
		if time.Now().Sub(date) > subDuration {
			ch.unregister <- client
		}
	}
}

func (ch *Channel) Unsub(c *Client) {
	ch.unregister <- c
}

func (ch *Channel) Subscribe(c *Client) {
	ch.register <- c
}

func MakeChannel(name string) *Channel {
	return &Channel{
		Name:       name,
		broadcast:  make(chan []byte),
		register:   make(chan *Client),
		unregister: make(chan *Client),
		clients:    make(map[*Client]time.Time),
		active:     true,
	}
}
