package main

import (
	"time"
)

type ServerStats struct {
	channels_current uint
	channels_created uint
	messages         uint
	users_current    uint
	users_registered uint
	started          time.Time
}

var Stats = &ServerStats{
	channels_current: 0,
	channels_created: 0,
	messages:         0,
	users_current:    0,
	users_registered: 0,
}

func GetStats() *ServerStats {
	if Stats == nil {
		Stats = &ServerStats{}
	}
	return Stats
}

func (s *ServerStats) Client(add bool) {
	if add {
		s.users_current++
		s.users_registered++
	} else {
		s.users_current--
	}
}

func (s *ServerStats) Channel(add bool) {
	if add {
		s.users_current++
		s.users_registered++
	} else {
		s.users_current--
	}
}

func (s *ServerStats) Message() {
	s.messages++
}

func (s *ServerStats) Start() {
	s.started = time.Now()
}
