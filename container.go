package main

import (
	"log"
)

type Container struct {
	loaded   bool
	clients  map[string]*Client
	channels map[string]*Channel
	stats    *ServerStats
	router   *Router
}

var container *Container = &Container{
	loaded: false,
}

func GetContainer() *Container {
	if container.loaded == false {
		container = &Container{
			loaded:   true,
			stats:    GetStats(),
			channels: make(map[string]*Channel),
			clients:  make(map[string]*Client),
		}
		container.stats.Start()
	}
	return container
}

func (c *Container) AddChannel(name string) {
	_, exists := c.channels[name]
	if exists == false {
		ch := MakeChannel(name)
		c.channels[name] = ch
		ch.run()
		c.stats.Channel(true)
	}
}

func (c *Container) RemoveChannel(name string) {
	_, exists := c.channels[name]
	if exists {
		c.channels[name].active = false
		c.stats.Channel(false)
	}
}

func (c *Container) GetChannel(name string) *Channel {
	ch, _ := c.channels[name]
	return ch
}

func (c *Container) GetClient(id string) *Client {
	cl, exists := c.clients[id]
	if exists {
		return cl
	} else {
		return nil
	}
}

func (c *Container) AddClient(id string, cl *Client) {
	c.clients[id] = cl
	c.stats.Client(true)
}

func (c *Container) RemoveClient(id string) {
	delete(c.clients, id)
	c.stats.Client(false)
}

func (c *Container) Save() {
	log.Println(c)
}
