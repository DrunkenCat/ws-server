package main

import (
	"fmt"
	"net/http"
	"time"
)

func main() {
	cnt := GetContainer()

	//defer cnt.Save()

	router := GetRouter(cnt)

	//router.r.Use(DisplayJson)

	go func(router *Router) {
		server := &http.Server{
			Handler:      router.http,
			Addr:         ":8080",
			ReadTimeout:  15 * time.Second,
			WriteTimeout: 15 * time.Second,
			IdleTimeout:  15 * time.Second,
		}
		fmt.Println(server.ListenAndServe())
	}(router)

	go func(router *Router) {
		server := &http.Server{
			Handler: router.ws,
			Addr:    ":9090",
		}
		fmt.Println(server.ListenAndServe())
	}(router)

	for {
		//time.Sleep(2 * time.Hour)
	}
}

// func DisplayJson(next http.Handler) http.Handler {
// 	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
// 		log.Println(w)
// 		log.Println(req)
// 		log.Println(ioutil.ReadAll(req.Response.Body))
// 		w.Header().Set("content-type", "application/json")
// 		next.ServeHTTP(w, req)
// 	})
// }
